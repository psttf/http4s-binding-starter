val Http4sVersion = "0.21.1"
val LogbackVersion = "1.2.3"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

lazy val app =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.psttf",
      scalaVersion := "2.12.10",
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "com.lihaoyi" %%% "scalatags" % "0.8.6",
        "org.typelevel" %% "cats-core" % "2.1.0",
        "io.circe" %%% "circe-generic" % "0.13.0",
        "io.circe" %%% "circe-literal" % "0.13.0",
        "io.circe" %%% "circe-generic-extras" % "0.13.0",
        "io.circe" %%% "circe-parser" % "0.13.0",
        "com.outr" %%% "scribe" % "2.7.10",
      ),
      scalacOptions ++= Seq(
        "-Xlint",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-Ypartial-unification",
        "-language:higherKinds"
      ),
      scalafmtOnCompile := true,
    )
    .jvmSettings(Seq(
    ))

lazy val appJS = app.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
    ),
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "1.0.0",
      "com.thoughtworks.binding" %%% "dom" % "11.9.0+56-3af5a32a",
      "org.scala-lang.modules" %% "scala-xml" % "1.2.0",
      ScalablyTyped.J.jquery,
      ScalablyTyped.B.bootstrap,
    ),
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full),
    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
    ),
    npmResolutions in Compile ++= Map(
      //    "snabbdom" -> "git://github.com/cornerman/snabbdom.git#semver:0.7.4",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("http4sbindingstarter.js.Main"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm
  )

lazy val appJVM = app.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    dockerExposedPorts := Seq(8080),
    dockerBaseImage := "oracle/graalvm-ce:19.1.1",
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(appJS, Compile)).value,
    mappings.in(Universal) ++= webpack.in(Compile, fullOptJS).in(appJS, Compile).value.map { f =>
      f.data -> s"assets/${f.data.getName}"
    },
    mappings.in(Universal) ++= Seq(
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "css" / "bootstrap.min.css" ->
        "assets/bootstrap/dist/css/bootstrap.min.css",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "jquery" / "dist" / "jquery.slim.min.js" ->
        "assets/jquery/dist/jquery.slim.min.js",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "popper.js" / "dist" / "umd/popper.min.js" ->
        "assets/popper.js/dist/umd/popper.min.js",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "js/bootstrap.min.js" ->
        "assets/bootstrap/dist/js/bootstrap.min.js",
    ),
    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    mainClass in reStart := Some("http4sbindingstarter.Main"),
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % "2.1.1",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "com.github.pureconfig" %% "pureconfig" % "0.12.2",
      "org.slf4j" % "slf4j-nop" % "1.7.30",
    ),
  )

disablePlugins(RevolverPlugin)

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}

herokuAppName in Compile := "http4s-binding-starter"

target in Compile := (target in(appJVM, Compile)).value
