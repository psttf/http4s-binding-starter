package http4sbindingstarter

import cats.effect._
import cats.implicits._
import http4sbindingstarter.config.{AppConfig, SyncConfig}
import http4sbindingstarter.http._
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import pureconfig.ConfigSource
import scribe.{Level, Logger}

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      appConfig <- SyncConfig.read[IO, AppConfig](ConfigSource.default)
      exitCode  <- app(appConfig).use(_ => IO.never).as(ExitCode.Success)
    } yield exitCode

  private def app(appConfig: AppConfig): Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      httpApp = (
        staticEndpoints.endpoints() <+> HelloEndpoints.endpoints()
      ).orNotFound
      server <- BlazeServerBuilder[IO]
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS(httpApp))
        .resource
    } yield server

}
