package http4sbindingstarter.js

import com.thoughtworks.binding.Binding.Var
import com.thoughtworks.binding.{dom, Binding}
import http4sbindingstarter.models.Hello
import io.circe.parser._
import org.scalajs.dom.document
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.html.{Div, Input}
import org.scalajs.dom.raw.Event

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.xml.Elem

object HelloComponent {

  private lazy val protocolHost =
    if (document.location.protocol.startsWith("file"))
      "http://localhost:8080"
    else
      ""

  private def request(
    data: Var[String],
    value: String,
  ): Unit =
    Ajax
      .get(s"$protocolHost/hello?value=$value").foreach(
        xhr =>
          parse(xhr.responseText)
            .flatMap(_.as[Hello])
            .map(fill(data, _)),
      )

  private def fill(
    data: Var[String],
    hello: Hello,
  ): Unit =
    data.value =
      s"'${hello.value}' was said at ${hello.epochSecond} epoch second"

  def listGroupItemCssClass(danger: Boolean) =
    s"list-group-item list-group-item-${if (danger) "danger" else "success"}"

  @dom
  def render: Binding[Div] = {
    val data = Var("")
    val someTextInput: Input = {
      <input type="text" class="form-control mb-2" id="searchInputId"
        placeholder="Some Text" />
    }
    <div class="container" style="margin-top: 50px;">
      <div class="form-row align-items-center">
        <div class="col-auto">
          <label class="sr-only" for="searchInputId">Some Text</label>
          {someTextInput}
        </div>
        <div class="col-auto">
          <button type="submit" class="btn btn-primary mb-2"
            onclick={_: Event => request(data, someTextInput.value)}>Send</button>
        </div>
      </div>
      <div>
        Result:
      </div>
      <div>
        {data.bind}
      </div>
    </div>
  }

}
