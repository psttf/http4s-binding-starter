package http4sbindingstarter.js

import com.thoughtworks.binding.dom
import org.scalajs.dom.document

object Main {

  @dom
  def render = HelloComponent.render.bind

  def main(): Unit =
    dom.render(document.body, render)

}
